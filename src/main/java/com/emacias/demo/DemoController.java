package com.emacias.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	
	//Nuevo cambio en la clase
	@GetMapping({"/","/index","/home"})
	public String index() {
		return "Demo Microservicio";
	}

}
